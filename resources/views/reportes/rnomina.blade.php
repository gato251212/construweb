@extends('layouts.app')
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">
@endpush

@section('content')
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Informe") }}
                    </div>
                    <div class="card">
                        <div class="card-header">
                            {{ __("Mis desprendibles") }}
                        </div>
                        <div class="card-body">
                            <table 
                                class="ui celled table"  style="width:80%"
                             
                                id="correos-table"
                            >
                                <thead>
                                    <tr>
                                        <th>{{ __("ID") }}<br></th>
                                        <th>{{ __("Acciones") }}</th>
                                         <th>{{ __("Mes") }}<br></th>
                                        <th>{{ __("sueldo") }}</th> 
                                       </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('scripts')
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        let dt;
        jQuery(document).ready(function() {
            dt = jQuery("#correos-table").DataTable ({
                responsive:true,
                pageLength: 5,
                lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
                processing: true,
                serverSide: true,
              "scrollX": true,
      
                ajax: '{{ route('reporte.nomina') }}',
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
columns: [

                    {data: 'user_id', visible: false},
                     {data: 'actions'},
                  {data: 'mes'},
                    {data: 'sueldo'}
                ]


            });

       
        })
    </script>

    
@endpush