

<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Mi perfil") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('perfiles.index') }}"
        >
            {{ __("Cambiar contraseña") }}
        </a>
                <a class="dropdown-item" href="{{ route('perfiles.editar') }}"
        >
            {{ __("Editar") }}
        </a>
    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Cuadro de horas") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('empleado.horas') }}"
        >
            {{ __("Ingresar") }}
        </a>

    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Actividades") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('ractividad.misactividades') }}"
        >
            {{ __("Mis actividades") }}
        </a>
                <a class="dropdown-item" href="{{ route('actividad.actividad') }}"
        >
            {{ __("Agregar actividad") }}
        </a>
    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Desprendibles de nomina") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('nomina.reportes') }}"
        >
            {{ __("Mis desprendibles") }}
        </a>
             
    </div>
</li>





@include('segmento.navegacion.logged')