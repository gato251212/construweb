

<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Mi perfil") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('perfiles.index') }}"
        >
            {{ __("Cambiar contraseña") }}
        </a>
                <a class="dropdown-item" href="{{ route('perfiles.editar') }}"
        >
            {{ __("Editar") }}
        </a>
    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-expanded="false"
    >
       <span class="caret">{{ __("nomina") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('nomina.reportes2') }}"
        >
            {{ __("Ingresar") }}
        </a>

    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Provedores") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('rpedidos.pedidos') }}"
        >
            {{ __("Pedidos") }}
        </a>
                <a class="dropdown-item" href="{{ route('provedor.registrop') }}"
        >
            {{ __("Registrar pedido") }}
        </a>
                        <a class="dropdown-item" href="{{ route('provedor.registro') }}"
        >
            {{ __("Registrar provedor") }}
        </a>
    </div>
</li>



<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
       <span class="caret">{{ __("Productividad") }}</span>
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="#"
        >
            {{ __("Empleados") }}
        </a>
                  <a class="dropdown-item" href="#"
        >
            {{ __("Trabajadores") }}
        </a>   
    </div>
</li>





@include('segmento.navegacion.logged')