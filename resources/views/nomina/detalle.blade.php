@extends('layouts.app')

@section('content')

   <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Mis Desprendibles") }}
                    </div>
                    <div class="card-body">

        <div class="tab-content" style="margin-top:16px;">

  <div class="form-group">

  <div class="tab-pane active" id="panel_1">
      <div class="panel panel-default">
       <div class="panel-body">
                            <div class="stats">
                                @forelse($proyecto as  $caso)
<div class="stats"><span style="font-weight: bold;">{{ __("Nombre :") }} </span>{{$user->name}}</div>
  <div class="stats"><span style="font-weight: bold;">{{ __("Mes :") }} </span>{{$caso->mes}}</div>
  <div class="stats"><span style="font-weight: bold;">{{ __("Sueldo :") }} </span>{{$caso->sueldo}}</div>

                            </div>
        </div>
          <div  style="margin: 10px" >
         <a href="#" class="btn btn-primary">Certificado</a>
         </div>
        <br />
       </div>
      </div>
     </div>

 </div>
  </div>
   </div>

    </div>
     </div>
      </div>
        @empty
            <div class="alert alert-dark">
        <i class="fa fa-info-circle"></i>
        {{ __("No tienes mensajes") }}
    </div>
        @endforelse
 @endsection
 
