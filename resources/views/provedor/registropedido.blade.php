@extends('layouts.app')

@section('content')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<body>
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Ingresar compra") }}
                    </div>
                    <div class="card-body">
                         <form action="{{ route('Inscribir.actividad') }}" method="POST"onsubmit="return checkSubmit();">
                                @csrf
                            <ul class="nav nav-tabs">

    </ul>
        <div class="tab-content" style="margin-top:16px;">

  <div class="tab-pane active" id="panel_1">
      <div class="panel panel-default">
       <div class="panel-body">
        <div class="form-group">
        <div class="form-group">

         <label>Seleccione Provedor</label>
  <select name="juzgado_id" id="juzgado_id"   class="form-control" required> 
  <option value="0" disabled="true" selected="true">-Razon social-</option>
@foreach($activi as $juzgados)
    <option value="{{ $juzgados->id}}">{{$juzgados->empresa}}</option>
    @endforeach
  </select>
   <br />
   </div>
        <div class="form-group">

                          <div class="form-group">
         <label>Fecha entrega</label>
         <input type="text" name="fecha" id="fecha" class="form-control" />
         <span id="error_providencia" class="text-danger"></span>
        </div>
     </div>
        <label>Idfactura</label>
  <input type="text" name="valor_un" id="valor_un"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        
     </div>
      <div class="form-group">
     <label>Valor factura</label>
  <input type="text" name="valor_un" id="valor_un"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>

              <div class="form-group">
     <label>Valoracion</label>
  <input type="text" name="valor_un" id="valor_un"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>
  
        <br />
        <div align="center">

           <button type="submit" class="btn btn-primary">Inscribir</button>
        </div>
        <br />
       </div>
      </div>
     </div>
 </form>
 </div>
  </div>
   </div>
    </div>
     </div>
 </body>
@endsection

@push('scripts')


  <script type="text/javascript">

$(function () {
$("#fecha").datepicker();
});
</script>


<script type="text/javascript">
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Ant',
 nextText: 'Sig >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
 $(function(){
    $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true
    });
});
</script>


  <script type="text/javascript">

enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
    if (!enviando) {
        enviando= true;
        return true;
    } else {
        //Si llega hasta aca significa que pulsaron 2 veces el boton submit
        alert("El formulario ya se esta enviando");
        return false;
    }
}

</script>








</script>





    
@endpush

