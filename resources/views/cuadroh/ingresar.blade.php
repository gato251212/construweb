@extends('layouts.app')

@section('content')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<body>
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Cuadro de horas") }}
                    </div>
                    <div class="card-body">
                         <form action="{{ route('Inscribir.cuadro') }}" method="POST"onsubmit="return checkSubmit();">
                                @csrf
                            <ul class="nav nav-tabs">

    </ul>
        <div class="tab-content" style="margin-top:16px;">

  <div class="tab-pane active" id="panel_1">
      <div class="panel panel-default">
       <div class="panel-body">
        <div class="form-group">

                          <div class="form-group">
         <label>Dia laborado</label>
         <input type="text" name="fecha" id="fecha" class="form-control" />
         <span id="error_providencia" class="text-danger"></span>
        </div>
     </div>
             <div class="form-group">

         <label>Seleccione actividad</label>
  <select name="actividad" id="actividad"   class="form-control" required> 
  <option value="0" disabled="true" selected="true">-actividad-</option>
@foreach($actividades as $actividades)
    <option value="{{ $actividades->id}}">{{$actividades->nombre}}</option>

    @endforeach
  </select>

   <br />
   <div class="form-group">
<label>Nombre actividad</label>
  <input type="text" name="nombre" id="nombre"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />


   <div class="form-group">
  <input type="hidden" name="valor_unit" id="valor_unit"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">


      </div>
          <div class="form-group">
        <label>horas trabajadas</label>
  <input type="text" name="horas" id="horas"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>
                  <div class="form-group">
        <label>Metraje realizado</label>
  <input type="text" name="metro" id="metro"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>
        <br />
        <div align="center">

           <button type="submit" class="btn btn-primary">Inscribir</button>
        </div>
        <br />
       </div>
      </div>
     </div>
 </form>

      
           
                    <div class="card-header">
                        {{ __("Cuadro de horas") }}
                    </div>
                    <div class="card">
                        <div class="card-header">
                            {{ __("Horas registradas") }}
                        </div>
                        <div class="card-body">
                            <table
                                class="ui celled table"  style="width:100%"
                             
                                id="correos-table"
                            >
                                <thead>
                                    <tr>
                                        <th>{{ __("ID") }}<br></th>
                                        <th>{{ __("Actividad") }}<br></th>
                                        <th>{{ __("horas") }}<br></th>
                                        <th>{{ __("metraje") }}<br></th>
                                        <th>{{ __("Valor") }}<br></th>
                                        <th>{{ __("Acción") }}</th> 
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
 
   

 </div>
  </div>
   </div>
    </div>
     </div>
 </body>
@endsection
@push('scripts')
  <script type="text/javascript">

enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
    if (!enviando) {
        enviando= true;
        return true;
    } else {
        //Si llega hasta aca significa que pulsaron 2 veces el boton submit
        alert("El formulario ya se esta enviando");
        return false;
    }
}

</script>
  <script type="text/javascript">
 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Ant',
 nextText: 'Sig >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);

$(function () {
$("#fecha").datepicker();
});
</script>


<script type="text/javascript">

 $(function(){
    $("#fecha").datepicker({
        changeMonth: true,
        changeYear: true
    });
});
</script>


<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>



        let dt;
       var codigo = "";
       var var1 = "0";
         
        jQuery(document).on('change','#fecha',function (){
         
             codigo=document.getElementById('fecha').value;
         
            dt = jQuery("#correos-table").DataTable ({

                responsive:true,
                pageLength: 5,
                lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
                processing: true,
                serverSide: true,

      language: {
                  
                    url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
         ajax:{
           url : '{{ route('cuadro.enviado') }}',
           type: "get",
           data: {"estable": codigo}
         },
      
               
                
columns: [
                    {data: 'id', visible: false},
                    {data: 'actividad'},
                    {data: 'horas'},
                    {data: 'metraje'},
                     {data: 'valor'},
                     {data: 'actions'}
                ]


            });

        })



    </script>

<script type="text/javascript">
  $(document).ready(function(){


    $(document).on('change','#actividad',function () {
      var juz_id=$(this).val();
 var dir=$(this).parent();
      var ciu=$(this).parent();
      var juz=$(this).parent();
      var rad=$(this).parent();
      console.log(juz_id);
      var op="";
      $.ajax({
        type:'get',
        url:'{!!URL::to('caracti')!!}',
        data:{'id':juz_id},
        dataType:'json',//return data will be json
        success:function(data){
          console.log("nombre");
          console.log(data.nombre);
                    console.log("valor_unit");
          console.log(data.valor_unit);

          // here price is coloumn name in products table data.coln name

          dir.find('#nombre').val(data.nombre);
           ciu.find('#valor_unit').val(data.valor_unit);


        },
        error:function(){

        }
      });
    });

  });




</script>



   
@endpush
