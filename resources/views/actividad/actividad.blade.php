@extends('layouts.app')

@section('content')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<body>
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Inscribir actividad") }}
                    </div>
                    <div class="card-body">
                         <form action="{{ route('Inscribir.actividad') }}" method="POST"onsubmit="return checkSubmit();">
                                @csrf
                            <ul class="nav nav-tabs">

    </ul>
        <div class="tab-content" style="margin-top:16px;">

  <div class="tab-pane active" id="panel_1">
      <div class="panel panel-default">
       <div class="panel-body">
        <div class="form-group">
        <div class="form-group">

         <label>Seleccione actividad</label>
  <select name="juzgado_id" id="juzgado_id"   class="form-control" required> 
  <option value="0" disabled="true" selected="true">-Actividad-</option>
@foreach($activi as $juzgados)
    <option value="{{ $juzgados->id}}">{{$juzgados->nombre}}</option>
    @endforeach
  </select>
   <br />
<div class="form-group">
<label>Nombre actividad</label>
  <input type="text" name="nombre" id="nombre"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>
        <label>valor</label>
  <input type="text" name="valor_un" id="valor_un"  class="form-control"required  onkeyup="javascript:this.value=this.value.toUpperCase();">

 <br />

        </div>
     </div>
  
        <br />
        <div align="center">

           <button type="submit" class="btn btn-primary">Inscribir</button>
        </div>
        <br />
       </div>
      </div>
     </div>
 </form>
 </div>
  </div>
   </div>
    </div>
     </div>
 </body>
@endsection

@push('scripts')
  <script type="text/javascript">

enviando = false; //Obligaremos a entrar el if en el primer submit

function checkSubmit() {
    if (!enviando) {
        enviando= true;
        return true;
    } else {
        //Si llega hasta aca significa que pulsaron 2 veces el boton submit
        alert("El formulario ya se esta enviando");
        return false;
    }
}

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){


    $(document).on('change','#juzgado_id',function () {
      var juz_id=$(this).val();
 var dir=$(this).parent();
      var ciu=$(this).parent();
      var juz=$(this).parent();
      var rad=$(this).parent();
      console.log(juz_id);
      var op="";
      $.ajax({
        type:'get',
        url:'{!!URL::to('findPrice')!!}',
        data:{'id':juz_id},
        dataType:'json',//return data will be json
        success:function(data){
          console.log("nombre");
          console.log(data.nombre);
                    console.log("valor_un");
          console.log(data.valor_un);

          // here price is coloumn name in products table data.coln name

          dir.find('#nombre').val(data.nombre);
           ciu.find('#valor_un').val(data.valor_un);


        },
        error:function(){

        }
      });
    });

  });




</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

      $("#nameid").select2({
            placeholder: "Seleccionar",
            allowClear: true
        });
   $("#productname").select2({
            placeholder: "Seleccionar",
            allowClear: true
        });

</script>

    
@endpush

