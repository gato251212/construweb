<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();
 Route::get('caracti','CuadrohController@caracti')->name('caracti');
 Route::get('findPrice','ActividadController@findPrice')->name('findPrice');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(["prefix" => "perfiles", "middleware" => ["auth"]], function() {
	Route::get('/', 'PerfilesController@index')->name('perfiles.index');
	Route::get('/editar', 'PerfilesController@editar')->name('perfiles.editar');
		Route::put('/', 'PerfilesController@update')->name('perfiles.update');
	Route::put('/actualizar', 'PerfilesController@update')->name('perfiles.update2');

});

Route::group(["prefix" => "actividad", "middleware" => ["auth"]], function() {
	Route::get('/', 'ActividadController@index')->name('actividad.actividad');
	Route::get('actividads/rmisactividads', 'ActividadController@enviado')->name('actividad.enviado');
Route::get('/misactividades2', 'ActividadController@misactividades2')->name('ractividad.misactividades');
	Route::post('/actividad', 'ActividadController@agregar')->name('Inscribir.actividad');

	   Route::delete('/{actividads}/destroy', 'ActividadController@destroy')->name('actividad.destroy');

});



Route::group(["prefix" => "empleado", "middleware" => ["auth"]], function() {
	Route::get('/empleado/horas', 'CuadrohController@ingresar')->name('empleado.horas');
	Route::get('/empleado/buscar', 'CuadrohController@buscar')->name('empleado.buscar');
Route::get('/empleado/{id}', 'CuadrohController@detalle')->name('caso.detalle');
Route::post('/cuadro', 'CuadrohController@agregar')->name('Inscribir.cuadro');
Route::get('cuadro/empleado', 'CuadrohController@enviado')->name('cuadro.enviado');

Route::get('/reportes', 'NominaController@reportes')->name('nomina.reportes');
Route::get('/rjuzgados', 'NominaController@enviados')->name('reporte.nomina');

Route::get('/{nomina}', 'NominaController@detalle')->name('nomina.detalle');


});


Route::group(["prefix" => "admin", "middleware" => ["auth"]], function() {


Route::get('/reportes2', 'NominaController@reportes2')->name('nomina.reportes2');
Route::get('/rjuzgados2', 'NominaController@enviados2')->name('reporte.nomina2');
});


Route::group(["prefix" => "provedor", "middleware" => ["auth"]], function() {
Route::get('/', 'ProvedoreController@index')->name('provedor.registro');
Route::get('provedor/', 'ProvedoreController@rpedido')->name('provedor.registrop');
Route::get('/mispedidos', 'ProvedoreController@misactividades2')->name('rpedidos.pedidos');
	Route::get('pedidos/rmispedidos', 'ProvedoreController@enviado')->name('pedido.enviado');

});
