<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuadroh extends Model
{
    //
	 protected $table='cuadrohs';
  	protected $fillable = ['user_id','fecha_labor'];
      	  public function getRouteKeyName() {
		return 'fecha_labor';

}
}
