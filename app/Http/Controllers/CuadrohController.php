<?php

namespace App\Http\Controllers;
use App\Misactividades;
use App\Cuadroh;
use Illuminate\Http\Request;

class CuadrohController extends Controller
{

	    public function caracti(Request $request){
    
        //it will get price if its id match with product id
        $p=Misactividades::select('nombre','valor_unit')->where('id',$request->id)->first();
        
        return response()->json($p);
    }


	     public function enviado (Request $request) {
         $estable = $request->get('estable');

//return Datatables::of(User::where('id',=1)->get())->make(true);
$cuadroh = Cuadroh::select(['id','actividad','horas','metraje','valor','productividad'])
->where('user_id',auth()->user()->id)
->where('fecha_labor',$estable)

 
//$students = Correo::where('idUsuario',auth()->user()->id)

//$titles = DB::table('Correo')->pluck('idcorreo', 'destinatario', 'asunto', 'contenido_mail', 'idUsuario')
->get();


        $actions = 'botonera.datatables.actividad';



        return datatables()->of($cuadroh)->addColumn('actions', $actions)->rawColumns(['actions'])->toJson();
    }
    public function ingresar(){
         $actividades=Misactividades::select()//get data from table
         ->where('user_id',auth()->user()->id)
         ->get();
        return view('cuadroh.ingresar',compact('actividades'));


	}//

    public function buscar(Cuadroh $proyecto){
    $proyectos = Cuadroh::withCount([])
            ->where('user_id',auth()->user()->id);
      
        return view('cuadroh.bcuadro', compact('proyecto'));
    }
      


//



	    public function agregar(Request $request ){
         //funcion para agregar la educacion de cada usuario
	    	$horas=$request->input("horas");
	    	$metraje=$request->input("metro");
            $valor=$request->input("valor_unit");
	    	$total=$metraje/$horas;
	    	$total2=$metraje*$valor;
      $cuadroh= new Cuadroh;
         $cuadroh->user_id=auth()->user()->id;
         $cuadroh->fecha_labor= $request->input("fecha");
         $cuadroh->actividad_id=$request->input("actividad");
          $cuadroh->actividad=$request->input("nombre");
         $cuadroh->horas=$request->input("horas");
          $cuadroh->metraje=$request->input("metro");
          $cuadroh->productividad=$total;
           $cuadroh->valor=$total2;
         $resul= $cuadroh->save();

        if($resul){            
      return back()->with('message', ['success', __('Labor ingresada correctamente')]);
        }
        else
        {            
             return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  
        }

        
    }

}
