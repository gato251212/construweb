<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actividad;
use App\Misactividades;
class ActividadController extends Controller
{
     public function enviado () {
      
//return Datatables::of(User::where('id',=1)->get())->make(true);
$actividad = Misactividades::select(['id','nombre','valor_unit'])
->where('user_id',auth()->user()->id)
 
//$students = Correo::where('idUsuario',auth()->user()->id)

//$titles = DB::table('Correo')->pluck('idcorreo', 'destinatario', 'asunto', 'contenido_mail', 'idUsuario')
->get();


        $actions = 'botonera.datatables.actividad';



        return datatables()->of($actividad)->addColumn('actions', $actions)->rawColumns(['actions'])->toJson();
    }
    



        public function index()
    {
           $activi=Actividad::all();//get data from table
        return view('actividad.actividad',compact('activi'));

    } 

        public function misactividades2 () {
        $misactividades = Misactividades::withCount([])
            ->whereUserId(auth()->user()->id) ->paginate(5);

        return view('actividad.misactividades', compact('misactividades'));
    }



    public function agregar(Request $request ){
         //funcion para agregar la educacion de cada usuario
      $actividad= new Misactividades;
         $actividad->user_id=auth()->user()->id;
         $actividad->nombre= $request->input("nombre");
         $actividad->valor_unit=$request->input("valor_un");
         $actividad->id_actividad=$request->input("juzgado_id");
         $resul= $actividad->save();

        if($resul){            
            return redirect('/actividad/misactividades2')->with('message', ['success', __('actividad inscrito correctamente')]);
        }
        else
        {            
             return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  
        }

        
    }



    public function findPrice(Request $request){
    
        //it will get price if its id match with product id
        $p=Actividad::select('nombre','valor_un')->where('id',$request->id)->first();
        
        return response()->json($p);
    }





        public function findProductName(Request $request){

        
        //if our chosen id and products table prod_cat_id col match the get first 100 data 

        //$request->id here is the id of our chosen option id
        $data=Actividad::select('nombre','id')->where('cod_ciud',$request->id)->take(1000)->get();
        return response()->json($data);//then sent this data to ajax success
    }


      public function  detallew (Misactividades $misactividades,$id) {
     $misactividad = Misactividades::select('id','nombre','direccion','correo') 

      ->where('id','=',$id)        

         ->get();
        return view('actividades.detalle', compact('misactividad')); 
    }   

        public function update (Request $request) {
       $status = $request->input('id');
$proyectos =  Misactividades::find($status);
 $proyectos ->correo=$request->input("consecutivo");
  $proyectos ->direccion=$request->input("ano");

        $proyectos->save();

         return redirect('/actividad/misactividades')->with('message', ['success', __('actividad actualizado correctamente')]);
        }


    public function destroy ($id) {
        try {
            $misactividad=Misactividades::find($id);
            $misactividad->delete();
            return back()->with('message', ['success', __("actividad eliminado correctamente")]);
        } catch (\Exception $exception) {
            return back()->with('message', ['danger', __("Error eliminando el actividad")]);
        }
    }

}
