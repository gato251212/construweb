<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerfilesController extends Controller
{
    public function index () {
    	$user = auth()->user();
    	return view('perfiles.index', compact('user'));
    }

	public function editar () {
    	$user = auth()->user();
    	return view('perfiles.actualizar', compact('user'));
    }
public function update2 () {
		$this->validate(request(), [
			'password' => ['confirmed', new StrengthPassword]
		]);

		$user = auth()->user();
		$user->password = bcrypt(request('password'));
		$user->save();
	    return back()->with('message', ['success', __("Usuario actualizado correctamente")]);
    }

    public function update (Request $request) {
		$this->validate(request(), [
			'password' => ['confirmed', new StrengthPassword]
		]);

		$user = auth()->user();
		$user ->last_name=$request->input("last_name");
        $user ->name=$request->input("name");
        $user ->cedula=$request->input("cedula");
        $user ->tarjeta=$request->input("tarjeta");
		$user->celular=$request->input("celular");
		$user->direccion=$request->input("direccion");
		$user->domicilio=$request->input("domicilio");
		$user->save();
	    return back()->with('message', ['success', __("Usuario actualizado correctamente")]);
    }
    ////
}
