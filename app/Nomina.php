<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nomina extends Model
{
	 protected $table='nominas';
	protected $fillable = ['id','user_id'];

    //
    	public function getRouteKeyName() {
		return 'id';
	}

}
